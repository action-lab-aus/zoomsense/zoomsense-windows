$ProgressPreference = 'SilentlyContinue'
"Downloading Zoom C# SDK...";
Invoke-WebRequest "https://gitlab.com/action-lab-aus/zoomsense/zoomsense-sdk/-/raw/main/zoomsense-c-sharp-wrapper-main.zip" -OutFile zoomsdk.zip;

"Unzipping SDK"

expand-archive -path '.\zoomsdk.zip' -destinationpath '.\'

"Moving Files"

Copy-Item .\zoomsense-c-sharp-wrapper-main\bin .\bin\Release -recurse
