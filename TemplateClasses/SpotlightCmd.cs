﻿using System;

namespace ZoomSensor.TemplateClasses
{
    class SpotlightCmd
    {
        public string status { get; set; }
        public uint uid { get; set; }
        public DateTime completed { get; set; }

        public SpotlightCmd(string status, uint uid, string allowUnmute)
        {
            this.status = status;
            this.uid = uid;
        }
    }
}
