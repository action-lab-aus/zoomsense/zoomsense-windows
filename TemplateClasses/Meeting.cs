﻿using System;

namespace ZoomSensor.TemplateClasses
{
    class Meeting
    {
        public string endTime { get; set; }
        public string startTime { get; set; }
        public string hostEmail { get; set; }
        public string hostName { get; set; }
        public string meetingType { get; set; }
        public Int32 noOfBreakoutRooms { get; set; }
        public string unitCode { get; set; }
    }
}
