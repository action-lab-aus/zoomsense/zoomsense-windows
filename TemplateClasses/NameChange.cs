﻿using System;

namespace ZoomSensor.TemplateClasses
{
    class NameChange
    {
        public NameChange(string userId, string persistentUid, string userName, string userRole, bool isInBO, long timestamp)
        {
            this.userId = userId;
            this.persistentUid = persistentUid;
            this.userName = userName;
            this.userRole = userRole;
            this.isInBO = isInBO;
            this.timestamp = timestamp;
        }

        public string userId { get; set; }
        public string persistentUid { get; set; }
        public string userName { get; set; }
        public string userRole { get; set; }
        public bool isInBO { get; set; }
        public long timestamp { get; set; }
    }
}
