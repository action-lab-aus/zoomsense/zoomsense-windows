﻿using System;

namespace ZoomSensor.TemplateClasses
{
    class ChatMessage
    {
        public ChatMessage(string msg, bool isInBO, UInt32 msgReceiver, string msgReceiverName
            , UInt32 msgSender, string msgSenderName, string msgType, long timestamp)
        {
            this.msg = msg;
            this.isInBO = isInBO;
            this.msgReceiver = msgReceiver;
            this.msgReceiverName = msgReceiverName;
            this.msgSender = msgSender;
            this.msgSenderName = msgSenderName;
            this.msgType = msgType;
            this.timestamp = timestamp;
        }

        public ChatMessage(string msg, bool isInBO, UInt32 msgReceiver, string msgReceiverName
            , UInt32 msgSender, string msgSenderName, long timestamp)
        {
            this.msg = msg;
            this.isInBO = isInBO;
            this.msgReceiver = msgReceiver;
            this.msgReceiverName = msgReceiverName;
            this.msgSender = msgSender;
            this.msgSenderName = msgSenderName;
            this.timestamp = timestamp;
        }

        public string msg { get; set; }
        public bool isInBO { get; set; }
        public UInt32 msgReceiver { get; set; }
        public string msgReceiverName { get; set; }
        public UInt32 msgSender { get; set; }
        public string msgSenderName { get; set; }
        public string msgType { get; set; }
        public long timestamp { get; set; }
    }
}
