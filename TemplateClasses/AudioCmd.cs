﻿using System;

namespace ZoomSensor.TemplateClasses
{
    class AudioCmd
    {
        public AudioCmd(string status, UInt32 uid, string allowUnmute)
        {
            this.status = status;
            this.uid = uid;
            this.allowUnmute = allowUnmute;
        }

        public string status { get; set; }
        public UInt32 uid { get; set; }
        public string allowUnmute { get; set; }
    }
}
