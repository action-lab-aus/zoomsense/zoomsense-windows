﻿namespace ZoomSensor.TemplateClasses
{
    class EntryExit
    {
        public EntryExit(string uid, string persistentUid, string username, string userRole
            , string status, bool isInBO, long timestamp)
        {
            this.uid = uid;
            this.persistentUid = persistentUid;
            this.username = username;
            this.userRole = userRole;
            this.status = status;
            this.isInBO = isInBO;
            this.timestamp = timestamp;
        }

        public string uid { get; set; }
        public string persistentUid { get; set; }
        public string username { get; set; }
        public string userRole { get; set; }
        public string status { get; set; }
        public bool isInBO { get; set; }
        public long timestamp { get; set; }
    }
}
