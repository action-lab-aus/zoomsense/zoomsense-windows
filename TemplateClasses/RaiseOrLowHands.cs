﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZoomSensor.TemplateClasses
{
    class RaiseOrLowHands
    {
        public RaiseOrLowHands(string uid, string username, string userRole
            , bool isLow, bool isInBO, long timestamp)
        {
            this.uid = uid;
            this.username = username;
            this.userRole = userRole;
            this.isLow = isLow;
            this.isInBO = isInBO;
            this.timestamp = timestamp;
        }

        public string uid { get; set; }
        public string username { get; set; }
        public string userRole { get; set; }
        public bool isLow { get; set; }
        public bool isInBO { get; set; }
        public long timestamp { get; set; }
    }
}
