﻿namespace ZoomSensor.TemplateClasses
{
    class DbUser
    {
        public DbUser(string persistentUid, string username, string userRole, long joinedAt)
        {
            this.persistentUid = persistentUid;
            this.username = username;
            this.userRole = userRole;
            this.joinedAt = joinedAt;
        }

        public string persistentUid { get; set; }
        public string username { get; set; }
        public string userRole { get; set; }
        public long joinedAt { get; set; }
    }
}
