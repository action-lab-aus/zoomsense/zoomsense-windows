﻿using System;

namespace ZoomSensor.TemplateClasses
{
    class ActiveHistory
    {
        // Zoom Id will a list since multiple active speakers can be detected
        public ActiveHistory(UInt32[] zoomid, long timestamp)
        {
            this.zoomid = zoomid;
            this.timestamp = timestamp;
        }

        public UInt32[] zoomid { get; set; }
        public long timestamp { get; set; }
    }
}
