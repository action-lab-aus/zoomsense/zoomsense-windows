﻿using System;

namespace ZoomSensor.TemplateClasses
{
    class EmojiItem
    {
        public EmojiItem(string emojiType, bool isInBO, UInt32 senderId, string senderName, string senderRole, long timestamp)
        {
            this.emojiType = emojiType;
            this.isInBO = isInBO;
            this.senderId = senderId;
            this.senderName = senderName;
            this.senderRole = senderRole;
            this.timestamp = timestamp;
        }

        public string emojiType { get; set; }
        public bool isInBO { get; set; }
        public UInt32 senderId { get; set; }
        public string senderName { get; set; }
        public string senderRole { get; set; }
        public long timestamp { get; set; }
    }
}
