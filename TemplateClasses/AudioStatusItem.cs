﻿using System;

namespace ZoomSensor.TemplateClasses
{
    class AudioStatusItem
    {
        public AudioStatusItem(string status, bool isInBO, UInt32 userId, string userRole, long timestamp)
        {
            this.status = status;
            this.isInBO = isInBO;
            this.userId = userId;
            this.userRole = userRole;
            this.timestamp = timestamp;
        }

        public string status { get; set; }
        public bool isInBO { get; set; }
        public UInt32 userId { get; set; }
        public string userRole { get; set; }
        public long timestamp { get; set; }
    }
}
