﻿using System;

namespace ZoomSensor.TemplateClasses
{
    class MessageCmd
    {
        public MessageCmd(string msg, UInt32 receiver)
        {
            this.msg = msg;
            this.receiver = receiver;
        }

        public string msg { get; set; }
        public UInt32 receiver { get; set; }
    }
}
