FROM mcr.microsoft.com/dotnet/framework/runtime:4.8

ADD https://aka.ms/vs/16/release/vc_redist.x86.exe redist.exe

RUN redist.exe /quiet /norestart /nocache

COPY ./bin/Release ./ZoomSensor

CMD start /wait C:\\ZoomSensor\\ZoomSensor.exe