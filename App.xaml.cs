﻿using CommandLine;
using NLog;
using NLog.Config;
using NLog.LayoutRenderers;
using System;
using System.Text;
using System.Windows;
using ZOOM_SDK_DOTNET_WRAP;

namespace ZoomSensor
{
    public partial class App : Application
    {
        public class Options
        {
            [Value(0, Required = true, HelpText = "Valid Zoom meeting id")]
            public string meetingNo { get; set; }
            [Value(1, Required = true, HelpText = "Display name of sensor")]
            public string displayName { get; set; }
            [Value(2, Required = true, HelpText = "Domain to the visualization")]
            public Uri websiteDomain { get; set; }
            [Value(3, Required = false, HelpText = "Valid Zoom meeting password")]
            public string password { get; set; }
            [Value(4, Required = false, HelpText = "Valid Zoom meeting start time")]
            public string startTime { get; set; }
            [Value(5, Required = false, HelpText = "Valid Zoom webinar token")]
            public string webinarToken { get; set; }
        }


        public class VersionRender : LayoutRenderer
        {
            protected override void Append(StringBuilder builder, LogEventInfo logEvent)
            {
                builder.Append(ThisAssembly.Git.BaseTag);
            }
        }

        static internal Options CurrentOptions;

        public static readonly NLog.Logger Logger;
        static App()
        {
            ConfigurationItemFactory.Default.LayoutRenderers.RegisterDefinition("gitversion", typeof(VersionRender));
            Logger = NLog.LogManager.GetCurrentClassLogger();
        }

        /**
         * Start the app with initialization of Zoom Sdk
         */
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            Logger.Info($"Starting ZoomSensor Application...");

            App.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;

            Parser.Default.ParseArguments<Options>(e.Args)
            .WithParsed<Options>(o =>
            {
                CurrentOptions = o;

                InitParam param = new InitParam
                {
                    web_domain = "https://zoom.us"
                };

                SDKError err = CZoomSDKeDotNetWrap.Instance.Initialize(param);
                if (SDKError.SDKERR_SUCCESS != err)
                {
                    Logger.Error("Initialize Error: " + err);
                    CZoomSDKeDotNetWrap.Instance.CleanUp();
                    Current.Shutdown(1);
                }
            }).WithNotParsed(errors =>
            {
                Logger.Error(errors);
                CZoomSDKeDotNetWrap.Instance.CleanUp();
                Current.Shutdown(1);
                Logger.Info("Shutdown ZoomSensor...");
            });
        }

        private void Current_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            Logger.Error("Dispatch Error: " + e.Exception.Message);
            e.Handled = true;
        }

        /**
         * Clean up SDK and shutdown the application
         */
        private void Application_Exit(object sender, ExitEventArgs e)
        {
            CZoomSDKeDotNetWrap.Instance.CleanUp();
            Current.Shutdown();
        }
    }
}
