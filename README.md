[![Build status](https://dev.azure.com/zoomsense/windowssensor/_apis/build/status/windowssensor-Docker%20container-CI)](https://dev.azure.com/zoomsense/windowssensor/_build/latest?definitionId=1)

![Docker Image Version (latest semver)](https://img.shields.io/docker/v/zoomsense/windowssensor?style=plastic)

`docker pull zoomsense/windowssensor`

# ZoomSense - Windows Sensor

The Windows Presentation Foundation application in this repository is developed using the official Zoom C# SDK. The application is designed to be run within a Windows Server Core Docker container on a VM infrastructure and act as Zoom clients that can be instructed to join meetings. Coordination of the sensors is via a containerised Node script - [**Docker Scheduler**](https://gitlab.com/action-lab-aus/zoomsense/zoomsense-docker). Multiple instances can be run on a VM within the dockerized environment.

Major features of Windows Sensor including:

- Observe real-time active speaker change events (including silence detection)
- Enable audio and video recordings within breakout rooms (by making sensors co-hosts or allow recording specifically)
- Push recordings to Firebase storage upon leaving the breakout rooms/meetings
- Receive and store chat histories within the Zoom meetings
- Enable `bye bye sensor` feature to kick out sensors within breakout rooms

# Run the Windows Sensor Locally

Pull the ZoomSense Windows Sensor from Docker:

```
docker pull zoomsense/windowssensor:latest
```

Windows Sensors can be run locally using the following docker command:

```
docker run zoomsense/windowssensor:latest -e <environment variables> start /wait C:\\ZoomSensor\\ZoomSensor.exe <meetingNo> <sensorId> <websiteDomain> <timestamp>
```

The following environment variables need to be set within each Windows Sensor:

```
FirebaseUrl=https://FIREBASE_DATABASE_URL.firebaseio.com
ZoomsenseUrl=https://FIREBASE_PROJECT_ID.web.app/#
ZoomAppKey=Zoom SDK App Key
ZoomAppSecret=Zoom SDK App Secret
FirebaseKey=Firebase App Key
FirebaseAuthEmail=Firebase Admin Account Auth Email
FirebaseAuthPwd=Firebase Admin Account Auth Password
FirebaseBucketUrl=FIREBASE_PROJECT_ID.appspot.com
ExternalIp=External IPv4 Address for the Windows VM/Local Machine
MeetingUid=Unique Firebase User ID
```

ZoomSense Docker Scheduler can be used to coordinate Windows Sensors if required. More details can be found under the [**ZoomSense Docker Scheduler Repository**](https://gitlab.com/action-lab-aus/zoomsense/zoomsense-docker).

# Azure CI

There is a build pipeline configured in Azure to create and publish the docker image whenever a commit is made to the master branch.  The sensor uses the
most recent tag on master as its version number, so any set of changes pushed to master should include an updated tag.

Azure does not support YAML configuration from GitLab (it needs specific integration with the Git server to read the YAML file before cloning the repo), so
the file `azure-pipelines.yml` in this repo is just for reference.

# Building the Windows Sensor

If you need to build customized logic on top of the existing ZoomSense Windows Sensor, you can use the following steps to get started.

## Dependencies

Download and unzip the [**Zoom C# Wrapper SDK**](https://marketplace.zoom.us/docs/sdk/native-sdks/windows/c-sharp-wrapper) into the same directory.

## 1. Restore NuGet Packages

Package Restore should happen automatically when opening the project. Navigate to `Tools > Options > NuGet Package Manager` for manual restore. For more information, please visit [**Restore Packages**](https://docs.microsoft.com/en-us/nuget/consume-packages/package-restore#restore-packages-manually-using-visual-studio).

## 2. Build Solution

Build the solution to compile the project files and components for building the Docker image.

## 3. Build Docker Image

[**Build the Docker Image**](https://docs.docker.com/engine/reference/commandline/build/) from the Dockerfile in the directory with:

```
docker build [OPTIONS] PATH | URL | -
```

## 4. Push Docker Image

[**Push the Docker Image**](https://docs.docker.com/engine/reference/commandline/push/) to the registry with:

```
docker push [OPTIONS] NAME[:TAG]
```
