﻿using System;
using System.Windows;
using System.ComponentModel;
using ZOOM_SDK_DOTNET_WRAP;

namespace ZoomSensor
{
    public partial class MainWindow : Window
    {
        JoinMeetingWindow joinMeetingWnd;

        public MainWindow()
        {
            InitializeComponent(); 
            Auth();
        }

        /**
         * Perform authentication for Zoom Sdk with App key and secret
         */ 
        private void Auth()
        {
            CZoomSDKeDotNetWrap.Instance.GetAuthServiceWrap().Add_CB_onAuthenticationReturn(onAuthenticationReturn);

            var appKey = Environment.GetEnvironmentVariable("ZoomAppKey");
            var appSecret = Environment.GetEnvironmentVariable("ZoomAppSecret");

            Console.WriteLine("the deets:");
            Console.WriteLine(appKey);
            Console.WriteLine(appSecret);

            if (appKey == null || appSecret == null)
            {
                App.Logger.Fatal("Zoom Sdk auth failed, please make sure the key and secret have been set up properly...");
                CZoomSDKeDotNetWrap.Instance.CleanUp();
                Application.Current.Shutdown(1);
                return;
            }

            AuthParam param = new AuthParam
            {
                appKey = appKey,
                appSecret = appSecret
            };

            CZoomSDKeDotNetWrap.Instance.GetAuthServiceWrap().SDKAuth(param);
            Hide();
        }

        /**
         * Listener for Zoom Sdk authentication and start joining the meeting
         */
        public void onAuthenticationReturn(AuthResult ret)
        {
            // If authentication successful, then start joining the meeting
            if (AuthResult.AUTHRET_SUCCESS == ret)
            {
                var meetingConfig = CZoomSDKeDotNetWrap.Instance.GetMeetingServiceWrap().GetUIController();
                if (joinMeetingWnd == null)
                    joinMeetingWnd = new JoinMeetingWindow();
                joinMeetingWnd.Show();
                joinMeetingWnd.JoinMeeting();
            }
            else
            {
                App.Logger.Fatal("Zoom Sdk Authentication Failed...");
                CZoomSDKeDotNetWrap.Instance.CleanUp();
                Application.Current.Shutdown(1);
            }
        }

        /**
         * Clean up SDK and shutdown the application
         */
        void Wnd_Closing(object sender, CancelEventArgs e)
        {
            CZoomSDKeDotNetWrap.Instance.CleanUp();
            Application.Current.Shutdown();
        }
    }
}
