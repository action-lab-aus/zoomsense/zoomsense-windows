﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Database.Query;
using Firebase.Storage;
using Newtonsoft.Json;
using Win32Process;
using ZOOM_SDK_DOTNET_WRAP;
using ZoomSensor.TemplateClasses;

namespace ZoomSensor
{
    public partial class JoinMeetingWindow : Window
    {
        // Meeting details from the command line args and env variables
        private string meetingNo;
        private string password;
        private string webinarToken;
        private string displayName;
        private string websiteDomain;
        private string ExternalIp;
        private string MeetingUid;
        private string startTime;

        // Firebase related attributes
        private FirebaseClient db;
        private FirebaseStorage storage;
        private FirebaseAuthProvider ap;
        private FirebaseAuth auth;
        private string activeCurrentNode;
        private string activeHistoryNode;
        private string chatNode;
        private string emojiNode;
        private string audioNode;
        private string nameChangeNode;
        private string leaveCommandNode;
        private string logEntryExitNode;
        private string raiseOrLowHandsNode;
        private string msgCommandNode;
        private string audioCommandNode;
        private string spotlightCommandNode;
        private string spotlightErrorNode;
        private string spotlightHistoryNode;
        private string meetingNode;
        private string promptTemplateNode;
        private string shareLink;
        private IDisposable[] firebasePathSubscriptions;

        // Meeting recording and uploading
        private static FileSystemWatcher watcher;
        private static Timer timer;
        private static Timer recordingTimer;
        private int recordingNumber = 0;
        private List<string> pendingUploadFiles = new List<string>();

        // Zoom SDK meeting services and controller
        IBOAttendeeDotNetWrap boAttendee;
        IMeetingServiceDotNetWrap meetingService;
        IMeetingChatControllerDotNetWrap chatController;
        IMeetingAudioControllerDotNetWrap audioController;
        IEmojiReactionControllerDotNetWrap emojiReactionController;
        IMeetingBOControllerDotNetWrap boController;
        IMeetingRecordingControllerDotNetWrap recordingController;
        IMeetingParticipantsControllerDotNetWrap participantController;
        IMeetingVideoControllerDotNetWrap videoController;

        private bool isInBO = false;
        private bool isCurrentlyRecordingFlag = false;
        private bool siteSent = false;
        private bool recordingMsgSent = false;
        private bool leaveMeeting = false;
        private int timerCount = 0;
        private int emptyMeetingCount;
        private long latestTimestamp;
        private DateTime endTime;
        private Dictionary<string, string> tempUserNameDict = new Dictionary<string, string>();
        private Dictionary<string, string> promptDic = new Dictionary<string, string>();
        private const string MANAGER_BOT = "ZoomSensor_1";
        private const string REC_PATH = "C:\\ZoomSensor";
        private static DateTime beginTime = new DateTime(1970, 1, 1);
        private HashSet<string> messageSet = new HashSet<string>();

        public JoinMeetingWindow()
        {
            meetingNo = App.CurrentOptions.meetingNo;
            password = App.CurrentOptions.password;
            webinarToken = App.CurrentOptions.webinarToken;
            displayName = App.CurrentOptions.displayName;
            websiteDomain = App.CurrentOptions.websiteDomain.ToString();
            startTime = App.CurrentOptions.startTime.Replace("-", "").Replace(":", "").Split('.')[0];
            websiteDomain = App.CurrentOptions.websiteDomain.ToString() + "_" + startTime;

            ExternalIp = Environment.GetEnvironmentVariable("ExternalIp");
            MeetingUid = Environment.GetEnvironmentVariable("MeetingUid");
            if (ExternalIp == null || MeetingUid == null)
            {
                App.Logger.Fatal("External Ip or Meeting Uid env not provided...");
                CZoomSDKeDotNetWrap.Instance.CleanUp();
                Application.Current.Shutdown(1);
                return;
            }

            InitializeDb();
            InitializeFileWatcher();
            InitializeTimer();
            InitializeComponent();

            var zTscoder = new ProcessInfo("zTscoder.exe");
            zTscoder.Started +=
                new ProcessInfo.StartedEventHandler(this.ZTscoderStarted);
        }

        /**
         * Initialise Firebase db and set up Firebase node paths
         */
        async public void InitializeDb()
        {
            // Initialize Firebase and authenticate using email and password
            var FirebaseKey = Environment.GetEnvironmentVariable("FirebaseKey");
            var FirebaseAuthEmail = Environment.GetEnvironmentVariable("FirebaseAuthEmail");
            var FirebaseAuthPwd = Environment.GetEnvironmentVariable("FirebaseAuthPwd");
            var FirebaseUrl = Environment.GetEnvironmentVariable("FirebaseUrl");
            var FirebaseBucketUrl = Environment.GetEnvironmentVariable("FirebaseBucketUrl");

            if (FirebaseKey == null || FirebaseAuthEmail == null 
                || FirebaseAuthPwd == null || FirebaseUrl == null || FirebaseBucketUrl == null)
            {
                App.Logger.Fatal("Firebase initialization failed, please ensure the credentials and urls have been set up properly...");
                CZoomSDKeDotNetWrap.Instance.CleanUp();
                Application.Current.Shutdown(1);
                return;
            }

            ap = new FirebaseAuthProvider(new FirebaseConfig(FirebaseKey));
            auth = await ap.SignInWithEmailAndPasswordAsync(FirebaseAuthEmail, FirebaseAuthPwd);

            // Initialize the Firebase db object
            db = new FirebaseClient(
                FirebaseUrl,
                new FirebaseOptions
                {
                    AuthTokenAsyncFactory = () => Task.FromResult(auth.FirebaseToken),
                }
            );

            // Initialize the Firebase storage object
            storage = new FirebaseStorage(
                FirebaseBucketUrl,
                new FirebaseStorageOptions
                {
                    AuthTokenAsyncFactory = () => Task.FromResult(auth.FirebaseToken),
                    ThrowOnCancel = true 
                });

            activeCurrentNode = $"data/activeSpeakers/{meetingNo}_{startTime}/{displayName}/current";
            activeHistoryNode = $"data/activeSpeakers/{meetingNo}_{startTime}/{displayName}/history";
            chatNode = $"data/chats/{meetingNo}_{startTime}/{displayName}";
            emojiNode = $"data/emoji/{meetingNo}_{startTime}/{displayName}";
            audioNode = $"data/audio/{meetingNo}_{startTime}/{displayName}";
            nameChangeNode = $"data/nameChange/{meetingNo}_{startTime}/{displayName}";
            promptTemplateNode = $"data/chats/prompts";
            leaveCommandNode = $"data/chats/{meetingNo}_{startTime}/{displayName}/leave";
            msgCommandNode = $"data/chats/{meetingNo}_{startTime}/{displayName}/message";
            audioCommandNode = $"data/audio/{meetingNo}_{startTime}/{displayName}/muteTasks";
            spotlightCommandNode = $"data/video/{meetingNo}_{startTime}/{displayName}/spotlightTasks";
            spotlightErrorNode = $"data/video/{meetingNo}_{startTime}/{displayName}/spotlightError";
            spotlightHistoryNode = $"data/video/{meetingNo}_{startTime}/{displayName}/spotlightHistory";
            meetingNode = $"meetings/{MeetingUid}/{meetingNo}_{startTime}/{displayName}";
            logEntryExitNode = $"data/logs/entryExit/{meetingNo}_{startTime}/{displayName}";
            raiseOrLowHandsNode = $"data/logs/raiseOrLowHands/{meetingNo}_{startTime}/{displayName}";

            try
            {
                var meetingDetails = await db.Child($"meetings/{MeetingUid}/{meetingNo}_{startTime}").OnceAsync<object>();
                foreach (var element in meetingDetails)
                {
                    if (element.Key.Equals("endTime"))
                        endTime = DateTime.Parse(element.Object.ToString());
                    if (element.Key.Equals("shareLink"))
                        shareLink = element.Object.ToString();
                }

                // Initialize the prompt template dictionary
                var promptTemplates = await db.Child(promptTemplateNode).OnceAsync<object>();
                foreach (var prompt in promptTemplates)
                {
                    var msg = prompt.Key;
                    var type = prompt.Object.ToString();
                    promptDic.Add(msg, type);
                }

                // Write sensor version number
                await db.Child($"meetings/{MeetingUid}/{meetingNo}_{startTime}/{displayName}/version").PutAsync<string>(ThisAssembly.Git.BaseTag);
                await db.Child($"{leaveCommandNode}/leave").PutAsync("false");

                if (displayName.Equals(MANAGER_BOT))
                    await db.Child($"meetings/{MeetingUid}/{meetingNo}_{startTime}/actualStartTime").PutAsync(DateTime.UtcNow);

                // Subscribe callbacks to various Firebase paths, and remember them for later unsubscription
                firebasePathSubscriptions = new []
                {
                    // Subscribe to leave command under the chat node
                    db.Child(leaveCommandNode)
                        .AsObservable<object>()
                        .Subscribe(async cmd =>
                        {
                            var cmdFlag = cmd.Object.ToString().ToUpper();
                            if (cmdFlag.Equals("TRUE"))
                            {
                                long kickOutTime = (DateTime.UtcNow.Ticks - beginTime.Ticks) / TimeSpan.TicksPerMillisecond;
                                await db.Child($"{activeCurrentNode}/kickOutTimestamp").PutAsync(kickOutTime);
                                await db.Child($"{leaveCommandNode}/leave").PutAsync("false");
                                App.Logger.Info("Firebase leave command node says to manually leave the breakout room.");
                                boAttendee = boController.GetBOAttendeeHelper();
                                boAttendee.LeaveBo();
                            }
                            if (cmdFlag.Equals("LEAVE_MEETING"))
                            {
                                long kickOutTime = (DateTime.UtcNow.Ticks - beginTime.Ticks) / TimeSpan.TicksPerMillisecond;
                                await db.Child($"{activeCurrentNode}/kickOutTimestamp").PutAsync(kickOutTime);
                                await db.Child($"{leaveCommandNode}/leave").PutAsync("false");
                                App.Logger.Info("Firebase leave command node says to manually leave the meeting.");
                                meetingService.Leave(LeaveMeetingCmd.LEAVE_MEETING);
                            }
                        }),

                    db.Child(msgCommandNode)
                        .AsObservable<MessageCmd>()
                        .Subscribe(async cmd => {
                            if (cmd.Object != null && cmd.EventType == Firebase.Database.Streaming.FirebaseEventType.InsertOrUpdate)
                            {
                                if (!messageSet.Contains(cmd.Key))
                                {
                                    messageSet.Add(cmd.Key);
                                    // `SendChatMsgTo()` needs to be called from the main UI thread.  See
                                    // https://devforum.zoom.us/t/sendchatto-not-working-inside-callback-function/18575
                                    await Application.Current.Dispatcher.InvokeAsync(() => SendMessageWithBackoff(cmd.Object.msg, cmd.Object.receiver));
                                    await db.Child($"{msgCommandNode}/{cmd.Key}").DeleteAsync();
                                }
                            }
                        }),

                    // Subscribe to audio control command under the audio node
                    db.Child(audioCommandNode)
                        .AsObservable<AudioCmd>()
                        .Subscribe(async cmd => {
                            if (cmd.Object != null)
                            {
                                if (cmd.Object.status == "mute")
                                {
                                    bool allowUnmute = cmd.Object.allowUnmute == "True";
                                    audioController.MuteAudio(cmd.Object.uid, allowUnmute);
                                }
                                else if (cmd.Object.status == "unmute")
                                    audioController.UnMuteAudio(cmd.Object.uid);
                                await db.Child($"{audioCommandNode}").DeleteAsync();
                            }
                        }),

                    // Subscribe to spotlight control command under the video node
                    db.Child(spotlightCommandNode)
                        .AsObservable<SpotlightCmd>()
                        .Subscribe(cmd =>
                        {
                            TryExecuteSpotlightCmd(cmd.Object);
                        })
                };

            }
            catch (Exception e)
            {
                App.Logger.Error("Initialize DB Error: " + e);
            }
        }

        /**
         * Zoom appears to have rate limits on messages sent in rapid succession, so apply exponential backoff if a
         * message fails to send.
         */
        private async Task SendMessageWithBackoff(string message, uint receiver)
        {
            var type = receiver == 0
                ? ChatMessageType.SDKChatMessageType_To_All
                : ChatMessageType.SDKChatMessageType_To_Individual;
            var retryCount = 10;
            var msDelay = 100;
            while (retryCount-- > 0)
            {
                var err = chatController.SendChatMsgTo(message, receiver, type);
                if (err == SDKError.SDKERR_SUCCESS)
                {
                    return;
                }
                await Task.Delay(msDelay);
                msDelay *= 2;
            }
        }
        
        private IUserInfoDotNetWrap GetSensorAsZoomUser()
        {
            uint[] participantIds = participantController.GetParticipantsList();

            foreach(uint id in participantIds)
            {
                IUserInfoDotNetWrap user = participantController.GetUserByUserID(id);
                if(user.GetUserNameW() == this.displayName)
                {
                    // this is probably us...
                    return user;
                }
            }

            return null;
        }

        private bool IsCoHost()
        {
            var sensor = GetSensorAsZoomUser();

            return (sensor != null &&
                (sensor.GetUserRole() == UserRole.USERROLE_HOST ||
                 sensor.GetUserRole() == UserRole.USERROLE_COHOST));
        }

        private string GetErrorJson(string code, string cmd)
        {
            return JsonConvert.SerializeObject(new Error
            {
                time = DateTime.UtcNow,
                code = code,
                command = cmd
            });
        }

        private async void TryExecuteSpotlightCmd(SpotlightCmd cmd)
        {
            try
            {
                if (cmd == null) return;

                // only bother if 3 or more participants
                if(participantController.GetParticipantsList().Length < 3)
                {
                    App.Logger.Info("Not enough participants for spotlight");
                    await db.Child($"{spotlightErrorNode}").PutAsync(GetErrorJson("COUNT", cmd.status));
                    return;
                }

                // only continue if (co)host
                if(!IsCoHost())
                { 
                    App.Logger.Info("Not co/host, can't spotlight");
                    await db.Child($"{spotlightErrorNode}").PutAsync(GetErrorJson("PERMS", cmd.status));
                    return;
                }

                SDKError result = SDKError.SDKERR_UNKNOWN;

                if (cmd.status == "spotlight")
                {
                    result = videoController.SpotlightVideo(cmd.uid);
                }
                else if (cmd.status == "unspotlight")
                {
                    result = videoController.UnSpotlightVideo(cmd.uid);
                }

                App.Logger.Info($"{cmd.status} for user {cmd.uid} result: {result}");

                if(result == SDKError.SDKERR_SUCCESS)
                {
                    cmd.completed = DateTime.UtcNow;
                    await db.Child($"{spotlightHistoryNode}").PostAsync(JsonConvert.SerializeObject(cmd));
                    _ = db.Child($"{spotlightCommandNode}").DeleteAsync();
                    _ = db.Child($"{spotlightErrorNode}").DeleteAsync();
                }
                else
                {
                    App.Logger.Info("Unknown error, likely invalid user ID");
                    await db.Child($"{spotlightErrorNode}").PutAsync(GetErrorJson("OTHER", cmd.status));
                    return;
                }
            }
            catch (Exception e)
            {
                App.Logger.Error("TryExecuteSpotlightCmd Error: " + e.Message);
            }
        }

        /**
         * Initialise FileSystemWatcher for zoom recordings
         */
        private void InitializeFileWatcher()
        {
            watcher = new FileSystemWatcher();
            watcher.Path = REC_PATH;
            watcher.Filter = "*.meetingRec";
            watcher.NotifyFilter = NotifyFilters.LastAccess
                                    | NotifyFilters.LastWrite
                                    | NotifyFilters.FileName
                                    | NotifyFilters.DirectoryName;
            watcher.IncludeSubdirectories = true;

            // Add file system event handlers.
            watcher.Created += OnRecordingDirectoryFileCreated;

            // Begin watching events
            watcher.EnableRaisingEvents = true;
        }

        /**
         * Initialize timer for trigger events every one minute
         */ 
        private void InitializeTimer()
        {
            // Create a timer and set a one minute interval
            timer = new Timer();
            timer.Interval = 1000 * 60;

            recordingTimer = new Timer();
            recordingTimer.Interval = 1000 * 3;

            // Hook up the Elapsed event for the timer
            timer.Elapsed += OnTimedEvent;

            // Have the timer fire repeated events
            timer.AutoReset = true;
            recordingTimer.AutoReset = true;

            // Start the timer
            timer.Enabled = true;
            recordingTimer.Enabled = true;
        }

        /**
         * Refresh firebase auth token before it expires
         */
        async private void RefreshToken()
        {
            try
            {
                var FirebaseUrl = Environment.GetEnvironmentVariable("FirebaseUrl");
                var FirebaseBucketUrl = Environment.GetEnvironmentVariable("FirebaseBucketUrl");

                auth = await ap.RefreshAuthAsync(auth);

                db = new FirebaseClient(
                    FirebaseUrl,
                    new FirebaseOptions
                    {
                        AuthTokenAsyncFactory = () => Task.FromResult(auth.FirebaseToken),
                    }
                );

                storage = new FirebaseStorage(
                    FirebaseBucketUrl,
                    new FirebaseStorageOptions
                    {
                        AuthTokenAsyncFactory = () => Task.FromResult(auth.FirebaseToken),
                        ThrowOnCancel = true,
                        HttpClientTimeout = TimeSpan.FromMinutes(10) // Default is 100 seconds
                });
            }
            catch (Exception e)
            {
                App.Logger.Error("Refresh Token Error: " + e);
            }
        }

        /**
         * Kill the zTscoder when it starts and start uploading recordings
         */ 
        private void ZTscoderStarted(object sender, EventArgs e)
        {
            App.Logger.Info("zTscoder.exe started...");
            foreach (var process in Process.GetProcessesByName("zTscoder"))
            {
                process.Kill();
            }
        }

        /**
         * Join meeting with meeting details as an API user
         */
        public void JoinMeeting()
        {
            RegisterCallBack();

            meetingService.GetMeetingConfiguration().PrePopulateWebinarRegistrationInfo("zoomsense@actionlab.dev", displayName);

            // Disable audio and video for the Zoom Sensor
            JoinParam4WithoutLogin joinParam = new JoinParam4WithoutLogin
            {
                meetingNumber = UInt64.Parse(meetingNo),
                psw = password,
                userName = displayName,
                isVideoOff = true,
                isAudioOff = true
            };

            if (webinarToken != null) joinParam.webinarToken = webinarToken;

            JoinParam param = new JoinParam
            {
                userType = SDKUserType.SDK_UT_WITHOUT_LOGIN,
                withoutloginJoin = joinParam,
            };

            // Join meeting with the parameters defined
            SDKError result = CZoomSDKeDotNetWrap.Instance.GetMeetingServiceWrap().Join(param);
            if (SDKError.SDKERR_SUCCESS == result)
            {
                Hide();
            }
            else
            {
                App.Logger.Fatal("Join Zoom Meeting Failed: " + result);
                CZoomSDKeDotNetWrap.Instance.CleanUp();
                Application.Current.Shutdown(1);
            }    
        }

        /**
         * Register callbacks for meeting events listeners
         */
        private void RegisterCallBack()
        {
            meetingService = CZoomSDKeDotNetWrap.Instance.GetMeetingServiceWrap();
            meetingService.Add_CB_onMeetingStatusChanged(OnMeetingStatusChanged);

            chatController = meetingService.GetMeetingChatController();
            chatController.Add_CB_onChatMsgNotifcation(OnChatMsgNotifcation);

            audioController = meetingService.GetMeetingAudioController();
            audioController.Add_CB_onUserActiveAudioChange(OnUserActiveAudioChange);
            audioController.Add_CB_onUserAudioStatusChange(OnUserAudioStatusChange);

            boController = meetingService.GetMeetingBOController();
            boAttendee = boController.GetBOAttendeeHelper();

            recordingController = meetingService.GetMeetingRecordingController();
            recordingController.Add_CB_onRecordingStatus(OnRecordingStatus);
            recordingController.Add_CB_onRecordPriviligeChanged(OnRecordPrivilegeChanged);

            participantController = meetingService.GetMeetingParticipantsController();
            participantController.Add_CB_onUserJoin(OnUserJoin);
            participantController.Add_CB_onUserLeft(OnUserLeft);
            participantController.Add_CB_onLowOrRaiseHandStatusChanged(OnLowOrRaiseHandStatusChanged);
            participantController.Add_CB_onUserNameChanged(OnUserNameChanged);
            participantController.Add_CB_onHostChangeNotification(OnHostChanged);
            participantController.Add_CB_onCoHostChangeNotification(OnCoHostChanged);

            emojiReactionController = meetingService.GetMeetingEmojiReactionController();
            emojiReactionController.Add_CB_onEmojiReactionReceived(OnEmojiReactionReceived);

            videoController = meetingService.GetMeetingVideoController();
            videoController.Add_CB_onSpotlightVideoChangeNotification(OnSpotlightChanged);
            
        }

        private void OnCoHostChanged(uint userId, bool isCoHost)
        {
            App.Logger.Info("CoHost changed " + JsonConvert.SerializeObject(userId) + " " + isCoHost);
            if(IsCoHost())
            {
                CheckForSpotlightCmds();
            }

        }

        private void OnHostChanged(uint userId)
        {
            App.Logger.Info("Host changed " + JsonConvert.SerializeObject(userId));
            if (IsCoHost())
            {
                CheckForSpotlightCmds();
            }
        }

        private async void CheckForSpotlightCmds()
        {
            IReadOnlyCollection<FirebaseObject<SpotlightCmd>> spotCmds = await db.Child(spotlightCommandNode).OnceAsync<SpotlightCmd>();
            foreach (FirebaseObject<SpotlightCmd> cmd in spotCmds)
            {
                TryExecuteSpotlightCmd(cmd.Object);
            }
        }

        private void OnSpotlightChanged(uint[] lstSpotlightedUserID)
        {
            if(lstSpotlightedUserID == null)
            {
                App.Logger.Info("Spotlight removed");
            }
            else
            {
                App.Logger.Info("Spotlighted " + JsonConvert.SerializeObject(lstSpotlightedUserID));
            }
            
        }

        private bool CanStartRecording()
        {
            return recordingController.CanStartRecording(false, 0) == SDKError.SDKERR_SUCCESS;
        }

        /**
         * Implement onMeetingStatusChanged which gets meeting status changes (e.g. leave breakout room)
         */
        private async void OnMeetingStatusChanged(MeetingStatus status, int iResult)
        {
            try
            {
                App.Logger.Info($"Meeting status changed: { status.ToString() }");

                switch (status)
                {
                    case MeetingStatus.MEETING_STATUS_JOIN_BREAKOUT_ROOM:
                        JoinBreakoutRoom();
                        break;
                    case MeetingStatus.MEETING_STATUS_LEAVE_BREAKOUT_ROOM:
                        isInBO = false;
                        await db.Child($"{chatNode}/isInBO").PutAsync(false);
                        await StopRecordingIfApplicable();

                        if (!CanStartRecording())
                            await MoveActiveSpeakerToHistory();
                        break;
                    case MeetingStatus.MEETING_STATUS_ENDED:
                        App.Logger.Info("Leaving Meeting...");
                        leaveMeeting = true;
                        timer.Enabled = false;
                        recordingTimer.Enabled = false;
                        // Stopping the timers above also halts Firebase token refreshes, so unsubscribe the Firebase
                        // path listeners (they have nothing to do now the meeting has finished, and they'll cause
                        // permission errors if the token expires).
                        foreach (var subscription in firebasePathSubscriptions)
                        {
                            subscription.Dispose();
                        }
                        await db.Child($"{chatNode}/isInBO").PutAsync(false);
                        await CleanResources();
                        if (pendingUploadFiles.Count == 0)
                        {
                            App.Logger.Info("No recording files remain to upload, shutting down.");
                            Application.Current.Shutdown(0);
                        }
                        break;
                    case MeetingStatus.MEETING_STATUS_INMEETING:
                        // Add chat message notification callback
                        chatController = meetingService.GetMeetingChatController();
                        chatController.Remove_CB_onChatMsgNotifcation(OnChatMsgNotifcation);
                        chatController.Add_CB_onChatMsgNotifcation(OnChatMsgNotifcation);
                        _ = StartRecordingIfApplicable();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                App.Logger.Error("onMeetingStatusChanged Error: " + e);
            }
        }

        /**
         * Implement onUserActiveAudioChange which listens to active speaker changes
         */
        async private void OnUserActiveAudioChange(UInt32[] isActiveAudio)
        {
            try
            {
                if (isInBO || displayName.Equals(MANAGER_BOT))
                {
                    // Calculate the timestamp with begin time value
                    long timestamp = (DateTime.UtcNow.Ticks - beginTime.Ticks) / TimeSpan.TicksPerMillisecond;
                    ActiveHistory activeHistory = new ActiveHistory(isActiveAudio, timestamp);

                    var currentSpeakers = await db.Child(activeCurrentNode).OnceAsync<object>();

                    bool hasBoFlag = false;
                    bool hasRecordingFlag = false;

                    foreach (var item in currentSpeakers)
                    {
                        if (item.Key.Equals("isInBO")) hasBoFlag = true;
                        if (item.Key.Equals("isRecording")) hasRecordingFlag = true;
                    }

                    if (!hasBoFlag)
                        await db.Child($"{activeCurrentNode}/isInBO").PutAsync(isInBO);
                    if (!hasRecordingFlag)
                    {
                        if (!CanStartRecording())
                            await db.Child($"{activeCurrentNode}/isRecording").PutAsync(false);
                        else
                        {
                            await db.Child($"{activeCurrentNode}/isRecording").PutAsync(true);
                            await db.Child($"{activeCurrentNode}/hasRecording").PutAsync(true);
                        }
                    }

                    await UpdateCurrentUserList();

                    await db.Child($"{activeCurrentNode}/activeHistory").PostAsync(activeHistory);
                }
            }
            catch (Exception e)
            {
                App.Logger.Error("onUserActiveAudioChange Error: " + e);
            }
        }

        /**
         * Implement onUserAudioStatusChange which listens to user audio status changes
         */
        async private void OnUserAudioStatusChange(IUserAudioStatusDotNetWrap[] lstaudiostatuschange)
        {
            try
            {
                if (isInBO || displayName.Equals(MANAGER_BOT))
                {
                    foreach (var statusChange in lstaudiostatuschange)
                    {
                        string status = statusChange.GetStatus().ToString();
                        UInt32 userId = statusChange.GetUserId();
                        string userRole = GetUserRole(getUserInfo(userId));
                        long timestamp = (DateTime.UtcNow.Ticks - beginTime.Ticks) / TimeSpan.TicksPerMillisecond;
                        AudioStatusItem audioStatus = new AudioStatusItem(status, isInBO, userId, userRole, timestamp);
                        await db.Child(audioNode).PostAsync(audioStatus);
                    }
                }
            }
            catch (Exception e)
            {
                App.Logger.Error("onUserAudioStatusChange Error: " + e);
            }
        }


        /**
         * Implement onChatMsgNotifcation which listens to new chat messages
         */
        async private void OnChatMsgNotifcation(IChatMsgInfoDotNetWrap chatMsg)
        {
            try
            {
                if (isInBO || displayName.Equals(MANAGER_BOT))
                {
                    string msg = chatMsg.GetContent();
                    UInt32 msgReceiver = chatMsg.GetReceiverUserId();
                    string msgReceiverName = chatMsg.GetReceiverDisplayName();
                    UInt32 msgSender = chatMsg.GetSenderUserId();
                    string msgSenderName = chatMsg.GetSenderDisplayName();
                    long timestamp = (DateTime.UtcNow.Ticks - beginTime.Ticks) / TimeSpan.TicksPerMillisecond;

                    // Verify whether the message is a high-level prompt message
                    // If so, add the promptType with the message
                    bool sent = false;
                    foreach (var prompt in promptDic)
                    {
                        string promptMsg = prompt.Key;
                        if (msg.StartsWith(promptMsg))
                        {
                            string promptType = prompt.Value;
                            ChatMessage newMsg = new ChatMessage(msg, isInBO, msgReceiver
                                , msgReceiverName, msgSender, msgSenderName, promptType, timestamp);
                            await db.Child(chatNode).PostAsync(newMsg);
                            sent = true;
                            break;
                        }
                    }

                    if (!sent)
                    {
                        ChatMessage newMsg = new ChatMessage(msg, isInBO, msgReceiver
                                , msgReceiverName, msgSender, msgSenderName, timestamp);
                        await db.Child(chatNode).PostAsync(newMsg);
                    }
                }
            }
            catch (Exception e)
            {
                App.Logger.Error("onChatMsgNotifcation Error: " + e);
            }
        }

        private IUserInfoDotNetWrap GetMeetingHost()
        {
            var userList = GetUserList();

            foreach (var userId in userList)
            {
                var userInfo = getUserInfo(userId);

                if (userInfo != null && GetUserRole(userInfo).Equals("Host"))
                {
                    return userInfo;
                }
            }

            return null;
        }

        /**
         * Implement joinBreakoutRoom which is called when breakout rooms are opened
         */
        async private void JoinBreakoutRoom()
        {
            await StopRecordingIfApplicable();
            try
            {
                if (displayName.Equals(MANAGER_BOT))
                {
                    if (!CanStartRecording() && !recordingMsgSent)
                    {
                        var host = GetMeetingHost();
                        if (host != null)
                        {
                            chatController.SendChatMsgTo(
                                $"Hi, I am your manager bot! If you want to enable recording for breakout rooms, please join the breakout room and make the sensor a co-host or enable `Allow Record`, thanks!",
                                host.GetUserID(), ChatMessageType.SDKChatMessageType_To_Individual);
                            recordingMsgSent = true;
                            
                        }
                    }
                }

                isInBO = true;
                await db.Child($"{chatNode}/isInBO").PutAsync(true);
                if (!CanStartRecording())
                    await MoveActiveSpeakerToHistory();
            }
            catch (Exception e)
            {
                App.Logger.Error("onBOStatusChanged Error: " + e);
            }
        }

        /**
         * Implement onRecordingStatus which listens recording start and stop events
         */
        async private void OnRecordingStatus(RecordingStatus status)
        {
            App.Logger.Info($"Recording status changed to {status}");
            try
            {
                if (status == RecordingStatus.Recording_Start)
                {
                    if (isInBO || !isInBO && displayName.Equals(MANAGER_BOT))
                    {
                        await db.Child($"{activeCurrentNode}/isRecording").PutAsync(true);
                        await db.Child($"{activeCurrentNode}/hasRecording").PutAsync(true);
                    }
                    else
                    {
                        // Stop recording if not in breakout room and not a manager bot
                        App.Logger.Info($"ZoomSensor not manager nor in breakout room, attempting to stop recording.");
                        isCurrentlyRecordingFlag = true;
                        await StopRecordingIfApplicable();
                    }
                }
            }
            catch (Exception e)
            {
                App.Logger.Error("onRecordingStatus Error: " + e);
            }
        }

        private async Task StartRecordingIfApplicable()
        {
            if ((isInBO || !isInBO && displayName.Equals(MANAGER_BOT) && webinarToken == null) &&
                !isCurrentlyRecordingFlag && CanStartRecording())
            {
                App.Logger.Info("Attempting to start recording.");
                // Create a new directory for saving the recordings for this phase of the meeting.
                var recordingPath = $"{REC_PATH}\\recording{++recordingNumber}";
                Directory.CreateDirectory(recordingPath);
                // Set path for video and audio recording
                var settingService = CZoomSDKeDotNetWrap.Instance.GetSettingServiceWrap();
                settingService.GetRecordingSettings().SetRecordingPath(recordingPath);
                App.Logger.Info($"Set recording path to {recordingPath}");

                ValueType timestamp = null;
                var result = recordingController.StartRecording(ref timestamp);
                if (result == SDKError.SDKERR_SUCCESS)
                {
                    App.Logger.Info($"Successfully started recording to {recordingPath}.");
                    isCurrentlyRecordingFlag = true;
                    var currentTimestamp = (DateTime.UtcNow.Ticks - beginTime.Ticks)
                                           / TimeSpan.TicksPerMillisecond;
                    await db.Child($"{activeCurrentNode}/recordingStart").PutAsync(currentTimestamp);
                    await db.Child($"{activeCurrentNode}/isRecording").PutAsync(true);
                    await db.Child($"{activeCurrentNode}/hasRecording").PutAsync(true);
                }
                else
                {
                    App.Logger.Error($"Start recording SDKError {result}");
                }
            }
        }

        private async Task StopRecordingIfApplicable()
        {
            if (isCurrentlyRecordingFlag)
            {
                App.Logger.Info($"Attempting to stop recording.");
                try
                {
                    ValueType timestamp = null;
                    var result = recordingController.StopRecording(ref timestamp);
                    isCurrentlyRecordingFlag = false;
                    if (result == SDKError.SDKERR_SUCCESS)
                    {
                        await db.Child($"{activeCurrentNode}/isRecording").PutAsync(false);
                    }
                    else
                    {
                        App.Logger.Info($"Attempt to stop recording returned error code {result}");
                    }
                }
                catch (Exception e)
                {
                    App.Logger.Error("Attempt to stop recording threw exception: " + e);
                }
            }
        }
       
        /**
         * Listen to recording privilege changes and start recording if applicable.
         */
        async private void OnRecordPrivilegeChanged(bool bCanRec)
        {
            if (bCanRec)
            {
                try
                {
                    App.Logger.Info("Record privilege changed - Start Recording");
                    await StartRecordingIfApplicable();
                }
                catch (Exception e)
                {
                    App.Logger.Error("Start recording exception: " + e.Message);
                }
            }
        }

        /**
         * Update activeSpeakers and push an "enter" entryExit event into Firebase
         */
        private async void OnUserJoin(uint[] lstUserID)
        {
            try
            {
                if (isInBO || (!isInBO && displayName.Equals(MANAGER_BOT)))
                {
                    await UpdateCurrentUserList();
                }
                CheckForSpotlightCmds();
                await PushEntryExitData(lstUserID, "enter");
            }
            catch (Exception e)
            {
                App.Logger.Error("onUserJoin Error: " + e);
            }
        }

        /**
         * Push an "leave" entryExit event into Firebase
         */
        async private void OnUserLeft(uint[] lstUserID)
        {
            try
            {
                await PushEntryExitData(lstUserID, "leave");
            }
            catch (Exception e)
            {
                App.Logger.Error("onUserLeft Error: " + e);
            }
        }

        /**
         * Implement onLowOrRaiseHandStatusChanged when low or raise hand event is detected
         */
        async private void OnLowOrRaiseHandStatusChanged(bool bLow, uint userid)
        {
            try
            {
                if (isInBO || displayName.Equals(MANAGER_BOT))
                {
                    string uid = userid.ToString();
                    var userInfo = getUserInfo(userid);

                    if (userInfo != null)
                    {
                        string username = userInfo.GetUserNameW();
                        string userRole = GetUserRole(userInfo);
                        long timestamp = (DateTime.UtcNow.Ticks - beginTime.Ticks) / TimeSpan.TicksPerMillisecond;

                        string recordUserName = null;
                        if (username != null && !username.StartsWith("ZoomSensor_"))
                        {
                            tempUserNameDict[uid] = username;
                            recordUserName = username;
                        }
                        if (username == null)
                            tempUserNameDict.TryGetValue(uid, out recordUserName);


                        if (recordUserName != null)
                        {
                            RaiseOrLowHands raiseOrLowHands = new RaiseOrLowHands(uid, recordUserName, userRole, bLow, isInBO, timestamp);
                            await db.Child($"{raiseOrLowHandsNode}").PostAsync(raiseOrLowHands);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                App.Logger.Error("onLowOrRaiseHandStatusChanged Error: " + e);
            }
        }

        /**
         * Implement onUserNameChanged callback to push the name changing history to Firebase 
         */
        async private void OnUserNameChanged(uint userId, string userName)
        {
            try
            {
                if (isInBO || displayName.Equals(MANAGER_BOT))
                {
                    string uid = userId.ToString();
                    var userInfo = getUserInfo(userId);
                    var persistentUid = userInfo.GetPersistentId();
                    var userRole = GetUserRole(userInfo);
                    long timestamp = (DateTime.UtcNow.Ticks - beginTime.Ticks) / TimeSpan.TicksPerMillisecond;
                    NameChange nameChange = new NameChange(uid, persistentUid, userName, userRole, isInBO, timestamp);
                    await db.Child($"{nameChangeNode}").PostAsync(nameChange);
                }
            }
            catch (Exception e)
            {
                App.Logger.Error("onUserNameChanged Error: " + e);
            }
        }

        /**
         * Implement onEmojiReactionReceived when emoji reactions are received. Currently, Zoom only supports
         * callbacks for the following 7 reactions: Clap, Heart, Joy, None, Openmounth, Tda, and Thumbsup
         */
        async private void OnEmojiReactionReceived(UInt32 senderid, EmojiReactionType type)
        {
            try
            {
                if (isInBO || displayName.Equals(MANAGER_BOT))
                {
                    string emojiType = type.ToString();
                    IUserInfoDotNetWrap userInfo = getUserInfo(senderid);
                    string senderName = userInfo.GetUserNameW();
                    string senderRole = GetUserRole(userInfo);
                    long timestamp = (DateTime.UtcNow.Ticks - beginTime.Ticks) / TimeSpan.TicksPerMillisecond;
                    EmojiItem newEmojiItem = new EmojiItem(emojiType, isInBO, senderid, senderName, senderRole, timestamp);
                    await db.Child(emojiNode).PostAsync(newEmojiItem);
  
                }
            }
            catch (Exception e)
            {
                App.Logger.Error("onEmojiReactionReceived Error: " + e);
            }
        }

        /**
         * Push user entry and exit data into Firebase (triggered by onUserJoin & onUserLeft callbacks)
         */
        private async Task PushEntryExitData(uint[] lstUserID, string status)
        {
            try
            { 
                if (isInBO || displayName.Equals(MANAGER_BOT))
                {
                    foreach (uint userId in lstUserID)
                    {
                        string uid = userId.ToString();
                        var userInfo = getUserInfo(userId);
                        
                        if (userInfo != null)
                        {
                            var persistentUid = userInfo.GetPersistentId();
                            string username = userInfo.GetUserNameW();
                            string userRole = GetUserRole(userInfo);
                            long timestamp = (DateTime.UtcNow.Ticks - beginTime.Ticks) / TimeSpan.TicksPerMillisecond;

                            string recordUserName = null;
                            if (username != null && !username.StartsWith("ZoomSensor_"))
                            {
                                tempUserNameDict[uid] = username;
                                recordUserName = username;
                            }
                            if (username == null)
                                tempUserNameDict.TryGetValue(uid, out recordUserName);


                            if (recordUserName != null)
                            {
                                EntryExit entryExit = new EntryExit(uid, persistentUid, recordUserName, userRole, status, isInBO, timestamp);
                                await db.Child($"{logEntryExitNode}").PostAsync(entryExit);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                App.Logger.Error("Push Entry/Exit Data Error: " + e);
            }
        }

        /**
         * Timer event to update the lastSeen field in the scheduling node to indicate whether the bot 
         * is still actively working or not
         */
        private async void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            try
            {
                timerCount++;
                if (timerCount >= 55)
                {
                    RefreshToken();
                    timerCount = 0;
                }

                var lastSeen = DateTime.UtcNow;
                await db.Child($"scheduling/{ExternalIp}/sensors/{meetingNo}_{startTime}-{displayName.Split('_')[1]}")
                    .Child("lastSeen").PutAsync(lastSeen);

                // Test if the meeting was scheduled to end more than 10 minutes ago.
                var diff = endTime.Subtract(lastSeen).TotalMinutes;
                if (diff > -10)
                    return; // No, it's still going, don't check for empty meeting.
                
                var userList = GetUserList();
                var meetingHasPeople = userList != null && userList.Any((userId) =>
                {
                    var username = getUserInfo(userId)?.GetUserNameW();
                    return username != null && !username.StartsWith("ZoomSensor_");
                });

                if (meetingHasPeople)
                {
                    emptyMeetingCount = 0;
                }
                else if (++emptyMeetingCount > 5)
                {
                    // Leave meeting if there is no active users for 5 consecutive minutes
                    App.Logger.Info("No active users - manually leaving meeting.");
                    meetingService.Leave(LeaveMeetingCmd.LEAVE_MEETING);
                }
            }
            catch (Exception eventException)
            {
                App.Logger.Error("OnTimedEvent Error: " + eventException);
            }
        }

        /**
         * Event which listens the creation of .meetingrec files
         */
        private void OnRecordingDirectoryFileCreated(object source, FileSystemEventArgs e)
        {
            var shouldStartUpload = (pendingUploadFiles.Count == 0);
            App.Logger.Info($"Detected recording file created at path {e.FullPath}");
            pendingUploadFiles.Add(e.FullPath);
            if (Path.GetFileName(e.FullPath) == "double_click_to_convert_01.meetingrec")
            {
                // Also add the zoomver.tag file, which the file watcher doesn't report (because it can only have one
                // filename pattern ):
                var directory = Path.GetDirectoryName(e.FullPath);
                pendingUploadFiles.Add($"{directory}{Path.DirectorySeparatorChar}zoomver.tag");
            }
            if (shouldStartUpload)
            {
                _ = UploadRecordings();
            }
        }

        /**
         * Upload the recordings to Firebase storage
         */
        private async Task UploadRecordings()
        {
            var tryCount = 0;
            while (pendingUploadFiles.Count > 0)
            {
                var filename = pendingUploadFiles[0];
                try
                {
                    // Open the file and upload
                    using (var stream = File.Open(filename, FileMode.Open, FileAccess.Read))
                    {
                        // Construct FirebaseStorage with the path of the recording folder
                        var task = storage
                            .Child(MeetingUid)
                            .Child($"{meetingNo}_{startTime}")
                            .Child($"{displayName}-{latestTimestamp}")
                            .Child(Path.GetFileName(filename))
                            .PutAsync(stream);
                        task.Progress.ProgressChanged += (s, e) => App.Logger.Info($"Uploading file {filename}: {e.Percentage} %");
                        // Wait until upload completes and get the download url
                        var downloadUrl = await task;
                        App.Logger.Info($"Download Url for uploaded file {filename}: " + downloadUrl);
                        pendingUploadFiles.RemoveAt(0);
                    }
                }
                catch (Exception e)
                {
                    if (!(e is IOException) || !e.Message.EndsWith(" because it is being used by another process."))
                    {
                        if (++tryCount >= 30)
                        {
                            App.Logger.Error($"Uploading file {filename} encountered too many unrecognised exceptions, last one: " + e);
                            pendingUploadFiles.RemoveAt(0);
                        }
                        else
                        {
                            App.Logger.Info("Error trying to upload recording (will retry).  Reason: " + e.Message);
                        }
                    }
                    Task.Delay(1000).Wait();
                }
            }
            if (leaveMeeting)
            {
                App.Logger.Info($"Meeting has finished and all files uploaded, shutting down.");
                Application.Current.Dispatcher.Invoke(Application.Current.Shutdown);
            }
        }

        /**
         * Get the current user zoomid list from MeetingService
         */
        private UInt32[] GetUserList()
        {
            var meetingService = CZoomSDKeDotNetWrap.Instance.GetMeetingServiceWrap();
            var participantController = meetingService.GetMeetingParticipantsController();
            return participantController.GetParticipantsList();
        }

        /**
         * Get the current user info from ParticipantController
         */
        private IUserInfoDotNetWrap getUserInfo(UInt32 userId)
        {
            var meetingService = CZoomSDKeDotNetWrap.Instance.GetMeetingServiceWrap();
            var participantController = meetingService.GetMeetingParticipantsController();
            return participantController.GetUserByUserID(userId);
        }

        /**
         * Get the current user role from the user info
         */
        private string GetUserRole(IUserInfoDotNetWrap userInfo)
        {
            var userRole = userInfo.GetUserRole();
            switch (userRole)
            {
                case UserRole.USERROLE_HOST:
                    return "Host";
                case UserRole.USERROLE_COHOST:
                    return "Co-host";
                default:
                    return "Attendee";
            }
        }

        /**
         * Update the current user list for active speaker node when new users join the meetings
         */
        private async Task UpdateCurrentUserList()
        {
            try
            {
                UInt32[] userList = GetUserList();
                if (userList == null) return;

                CZoomSDKeDotNetWrap.Instance.GetMeetingServiceWrap().GetUIController().SwitchToVideoWall();

                var dbUserRaw = await db.Child($"{activeCurrentNode}/userList").OnceAsync<DbUser>();

                List<string> dbUserList = new List<string>();
                foreach (var dbUser in dbUserRaw)
                    dbUserList.Add(dbUser.Key);

                foreach (UInt32 userId in userList)
                {
                    string uid = userId.ToString();
                    var userInfo = getUserInfo(userId);

                    if (userInfo != null)
                    {
                        string username = userInfo.GetUserNameW();
                        if (!dbUserList.Contains(uid) && username != null && !username.StartsWith("ZoomSensor_"))
                        {
                            var persistentUid = userInfo.GetPersistentId();
                            string userRole = GetUserRole(userInfo);

                            if (userRole.Equals("Host") && displayName.Equals(MANAGER_BOT) && !siteSent)
                            {
                                var hostid = userInfo.GetUserID();
                                chatController.SendChatMsgTo("Please visit: " + shareLink + " to monitor your Zoom meeting engagements!", 
                                    hostid, ChatMessageType.SDKChatMessageType_To_Individual);
                                siteSent = true;
                            }

                            long joinedAt = (DateTime.UtcNow.Ticks - beginTime.Ticks) / TimeSpan.TicksPerMillisecond;
                            DbUser dbUser = new DbUser(persistentUid, username, userRole, joinedAt);
                            await db.Child($"{activeCurrentNode}/userList/{uid}").PutAsync(dbUser);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                App.Logger.Error("updateCurrentUserList Error: " + e);
            }
        }

        /**
         * Update the latest timestamp for the active history
         */
        private void UpdateTimestamp()
        {
            latestTimestamp = (DateTime.UtcNow.Ticks - beginTime.Ticks) / TimeSpan.TicksPerMillisecond; 
        }

        /**
         * If join/leave breakout rooms, push the current active speaker node to history
         */
        async private Task MoveActiveSpeakerToHistory()
        {
            try
            {
                UpdateTimestamp();
                long currentTimestamp = latestTimestamp;
                var current = await db.Child(activeCurrentNode).OnceAsync<object>();
                await db.Child(activeCurrentNode).DeleteAsync();

                foreach (var child in current)
                {
                    await db.Child($"{activeHistoryNode}/{currentTimestamp}/{child.Key}").PutAsync(child.Object);
                    if (child.Key.Equals("isInBO"))
                        await updateMeetingSession(child.Object);
                    if (child.Key.Equals("isRecording"))
                        await db.Child($"{activeHistoryNode}/{currentTimestamp}/isRecording").PutAsync(false);
                }
            }
            catch (Exception e)
            {
                App.Logger.Error("Move Active Speaker History Error: " + e);
            }
        }

        /**
         * If join/leave breakout rooms, update the meeting session info
         */
        async private Task updateMeetingSession(object boStatus)
        {
            try
            {
                await db.Child($"{meetingNode}/{latestTimestamp}/isInBO").PutAsync(boStatus);
            }
            catch (Exception e)
            {
                App.Logger.Error("Update Meeting Session Error: " + e);
            }
        }

        /**
         * Remove application callbacks and remove scheduling node
         */
        private async Task CleanResources()
        {
            try
            {
                await MoveActiveSpeakerToHistory();

                meetingService.Remove_CB_onMeetingStatusChanged(OnMeetingStatusChanged);
                chatController.Remove_CB_onChatMsgNotifcation(OnChatMsgNotifcation);
                audioController.Remove_CB_onUserActiveAudioChange(OnUserActiveAudioChange);
                audioController.Remove_CB_onUserAudioStatusChange(OnUserAudioStatusChange);
                recordingController.Remove_CB_onRecordingStatus(OnRecordingStatus);
                recordingController.Remove_CB_onRecordPriviligeChanged(OnRecordPrivilegeChanged);
                participantController.Remove_CB_onUserJoin(OnUserJoin);
                participantController.Remove_CB_onUserLeft(OnUserLeft);
                emojiReactionController.Remove_CB_onEmojiReactionReceived(OnEmojiReactionReceived);

                if (displayName.Equals(MANAGER_BOT))
                    await db.Child($"meetings/{MeetingUid}/{meetingNo}_{startTime}/actualEndTime").PutAsync(DateTime.UtcNow);
                await db.Child($"scheduling/{ExternalIp}/sensors/{meetingNo}_{startTime}-{displayName.Split('_')[1]}").DeleteAsync();
            }
            catch (Exception e)
            {
                App.Logger.Error("cleanResources Error: " + e);
            }
        }

        /**
         * Clean up SDK and shutdown the application
         */
        void Wnd_Closing(object sender, CancelEventArgs e)
        {
            CZoomSDKeDotNetWrap.Instance.CleanUp();
        }
    }
}
